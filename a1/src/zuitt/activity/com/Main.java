package zuitt.activity.com;
import java.util.ArrayList;
public class Main {
    public static void main(String[] args) {
        Phonebook newPhonebook = new Phonebook();


        Contact Newcontact1 = new Contact("karding", "09999999999", "mabini street");
        Contact Newcontact2 = new Contact("kanor", "09888888888", "rizal street");


        newPhonebook.addContact(Newcontact1);
        newPhonebook.addContact(Newcontact2);


        ArrayList<Contact> newcontacts = newPhonebook.getContacts();
        if (newcontacts.isEmpty()) {
            System.out.println("no information to show");
        } else {
            for (int i = 0; i < newcontacts.size(); i++) {
                Contact contactDetails = newcontacts.get(i);
                System.out.println("name: " + contactDetails.getName()
                        +" contact number: "+ contactDetails.getContactNumber()
                        +" address: "+ contactDetails.getAddress());
            }
        }

    }

}

