package com.zuitt.example;

public class Car {

//        Object-Oriented Concepts

    // Here are the definitions of the following:

    //Object - An abstract idea in your mind that represents something in the real world
    //Example: The concept of a dog
    //Class - The representation of the object using code
    //Example: Writing code that would describe a dog
    //Instance - A unique copy of the idea, made "physical"

//        objects
    /*
     * object are compose of 2 components
     * 1. states and attributes - what is the idea about
     * 2. behavior - what can idea do?
     *
     * example: a person has attribute like name, age and weight,
     * and a person can sleep and speak
     *
     * */

    /*
     * class creation
     *
     * a class is composed of four parts
     * 1. properties - characteristic of the object
     * 2. constructor - used to create an objects
     * 3. getters/setters - get and set the valuses of each property of the object
     * 4. methods - functions that an object can perform
     */

    private String name;
    private String brand;
    private int yearOfMake;

    //constructors
    /*empty constructor*/
    //private Car(){} //empty constructor

    //parameterize constructor
    private Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;

    }
    //getters and setters
    /*setters*/
    public void setName (String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        this.yearOfMake = yearOfMake;
    }

    public String getName() {
        return this.name;
    }

    public String getBrand() {
        return this.brand;
    }

    public int getYearOfMake() {
        return this.yearOfMake;
    }

    /*methods*/
    public void drive(){
        System.out.println("the car is running.");
    }

/*
* Access modifiers
* default - no keyword required
* private - only accessible within the class
* protected - only accessible to/with the classes (same package)
* public - can be accessed anywhere
*/

/*
* Fundamentals of OOP
* 1. encapsulation - ginagawa nating private
*   - mechanism of wrapping data (variables) and code acting on the data
* 2. inheritance - properties can be shared to sub-classes
* 3. abstraction - way of using function, process, codes without knowing it's origin
* 4. polymorphism -  passing attributes and can be added or modified
*/

    /*make a driver a component of car*/
    private Driver d;
    public Car(){
        this.d = new Driver("Alejandro");//whenever a new car is created it will have a driver named  Alejandro

    }
    public String getDriverName(){
        return this.d.getName();
    }



}
