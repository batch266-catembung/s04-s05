package com.zuitt.example;

public class StaticPoly {
    /*polymorphisim - the ability of code process to take many forms*/
    /*
    *1. static -manually constructed
    *2. dynamic - automatic construction/ modify in the process
    */

    public int addition(int a, int b){
        return a+b;
    }

    /*overload by changing the number of arguments*/
    public int addition(int a, int b, int c){
        return a+b+c;
    }

    public double addition (double a, double b){
        return a+b;
    }

}
