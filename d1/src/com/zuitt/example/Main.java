package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
//        System.out.println("this car is driven by "+ myCar.getDriverName());

//        Dog myPet = new Dog();
//        myPet.setName("brownie");
//        myPet.setColor("White");
//
//        myPet.speak();
//        System.out.println(myPet.getName() +" "+ myPet.getBreed()+" "+ myPet.getColor());

        Dog myDreamDog = new Dog();
        myDreamDog.setName("pinky");
        myDreamDog.setColor("Darkest black");
        myDreamDog.speak();
        System.out.println("my breed is, "+ myDreamDog.getBreed()+", my color is "+ myDreamDog.getColor());


        myCar.setName("Toyota");
        myCar.setBrand("vios");
        myCar.setYearOfMake(2025);

        System.out.println("Car name "+ myCar.getName());
        System.out.println("Car brand "+ myCar.getBrand());
        System.out.println("Car year of make "+ myCar.getYearOfMake());
        System.out.println("Car driver "+ myCar.getDriverName());

//        abstraction
        /*
        * is a process where all the logicg
        * and  complexity are hiddedn to the
        * user.
        * */

        Person child = new Person();
        child.sleep();
        child.run();

//        polymorphism
        /*
        * deliverd from greek word: "poly" means
        * and morph means "form"
        * */
        StaticPoly myAddition = new StaticPoly();
        System.out.println(myAddition.addition(3, 5));
        System.out.println(myAddition.addition(11, 11, 12));
        System.out.println(myAddition.addition(2.3, 9));


    }
}
