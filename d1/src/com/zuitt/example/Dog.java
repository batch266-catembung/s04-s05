package com.zuitt.example;

public class Dog extends Animal {
    private String breed;
    public Dog(){
        super();/*animal() Constructor*/
        this.breed = "Cerberus";
    }

    public Dog(String name, String color, String breed){
        super(name, color);/*Animal(String name, String color) -> constructor*/

        this.breed = breed;
    }

    /*getter*/
    public  String getBreed(){
        return  this.breed;
    }

    /*methods*/
    public void speak(){
        super.call(); /*the call() method from animal class*/
        System.out.println("rawr rawr");
    }



}
